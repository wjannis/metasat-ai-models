#!/usr/bin/python3

import tensorflow as tf


def tf2tflite(saved_model_dir, save_model=False):
    converter = tf.compat.v1.lite.TFLiteConverter.from_saved_model(saved_model_dir, input_shapes={"serving_default_input_1": [1,384,384,4]})

    tflite_model = converter.convert()

    if save_model:
        with open(saved_model_dir + '/../tflite/fp32/model.tflite', 'wb') as f:
            f.write(tflite_model)
    return tflite_model


if __name__=="__main__":
    path2model = "./keras"
    tf2tflite(path2model, save_model=True)
